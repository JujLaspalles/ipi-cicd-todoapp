// src/App.test.tsx
import { it, expect, vi } from 'vitest';
import { render, screen, fireEvent } from '@testing-library/react';
import TodoItem from '../TodoItem';

it('renders without crashing', () => {
  // ARRANGE
  const todoItem = {
    id: 1,
    name: 'Buy milk',
    done: false,
    createdAt: new Date().toISOString(),
  };
  render(
    <TodoItem
      item={todoItem}
      handleDeleteItem={() => {}}
      handleUpdateItem={() => {}}
    />
  );

  // Récupération de l'élément titre par son texte
  const nameElement = screen.getByText(/buy milk/i);

  // Assertions pour vérifier la présence du titre et sa classe CSS
  expect(nameElement).toBeInTheDocument();
  expect(nameElement).toHaveClass('TodoItem-name');
});

it('calls handleDeleteItem when checkbox is clicked', () => {
  // ARRANGE
  const todoItem = {
    id: 1,
    name: 'Buy milk',
    done: false,
    createdAt: new Date().toISOString(),
  };

  const handleUpdateItem = vi.fn();
  render(
    <TodoItem
      item={todoItem}
      handleDeleteItem={() => {}}
      handleUpdateItem={handleUpdateItem}
    />
  );

  // ACT
  const checkboxElement = screen.getByRole('checkbox');
  fireEvent.click(checkboxElement);

  // ASSERT
  expect(handleUpdateItem).toHaveBeenCalledWith(1, { name: 'Buy milk', done: true });
});
