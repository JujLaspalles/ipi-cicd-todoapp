CREATE DATABASE IF NOT EXISTS `app_todolist_backend` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
CREATE USER IF NOT EXISTS 'app_todolist_backend'@'localhost' IDENTIFIED BY 'app_todolist_backend';
GRANT ALL PRIVILEGES ON `app_todolist_backend`.* TO 'app_todolist_backend'@'localhost';
FLUSH PRIVILEGES;