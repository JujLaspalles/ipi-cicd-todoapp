import { queryAsync } from '../db';
import castDoneToBoolean from '../helpers/cast-done-to-boolean';

export async function readItems() {
  const items = await queryAsync('SELECT * FROM item');
  return items.map(castDoneToBoolean);
}

export async function readItemById(id) {
  const [item] = await queryAsync('SELECT * FROM item WHERE id = ?', [id]);
  return castDoneToBoolean(item);
}

export async function createItem(name) {
  const createdAt = new Date().toISOString();
  const { insertId } = await queryAsync('INSERT INTO item SET ?', {
    name,
    done: false,
    createdAt,
    updatedAt: createdAt,
  });
  return readItemById(insertId);
}

export async function updateItem(id, { name, done }) {
  const updatedAt = new Date().toISOString();
  await queryAsync('UPDATE item SET name = ?, done = ?, updatedAt = ? WHERE id = ?', [
    name,
    done,
    updatedAt,
    id,
  ]);
  return readItemById(id);
}

export async function deleteItem(id) {
  await queryAsync('DELETE FROM item WHERE id = ?', [id]);
}
